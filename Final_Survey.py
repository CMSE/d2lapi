import pandas as pd
import numpy as np


# This creates a unique order of questions for every quiz.
# Fixes/ used to fixed the following
# question numbers when random ordering on quiz is selected
# pooling errors
# used to determine "end" of questions for binary question format
# user_attempt is the data that you are attempting to transform/ modified (with answer key) question details file
# quiz answer is the answer key/question details file
def createSurveyOrder(survey_details):
    d = dict(enumerate(survey_details["Q Text"].unique(), 1))
    reversed_d = dict([(value, key) for key, value in d.items()])
    survey_details['label'] = survey_details['Q #'].astype(str) + survey_details.groupby(['User', 'Q #']). \
        cumcount().add(1).astype(str)
    survey_details.insert(9, "Q #", survey_details.pop("Q #"))
    survey_details['Q Text'] = survey_details['Q Text'].apply(lambda x: str(x).replace(u'\xa0', u''))

    return survey_details


# Read's in csvs probably can be removed in actual use since read_csv does it already.
def readincsv(survey_details):
    # survey_answer_details = pd.read_csv("Survey with names - Individual Attempts (2).csv")
    pass


def nullRowChecker(row, user_arr):
    if pd.notna(row):
        user_arr.append(row)
        return np.nan
    elif pd.isna(row):
        return user_arr[-1]


# append the "answerkey" to the top of the dataframe. (should be used before quiz order)
def csvWithUserColumn(survey_details):
    user_lst = []
    survey_details['Section #'] = survey_details['Section #'].apply(lambda x: nullRowChecker(x, user_lst))
    survey_details = survey_details[survey_details['Section #'].notna()]
    survey_details = survey_details.rename(columns={"Section #": "User"})
    return survey_details


# checks whether a True/False, multiple choice, or multi-select question is checked
# or not based on the response value
def trufalse_mc_check(response):
    return 1 if response == 1.000 else 0


# returns the answer for matching, ordering, short-answer, multiple-short answer or
# free-response questions
def answer_match_check(answer_match):
    return answer_match


# returns the answer for written-response questions
def answer_check(answer):
    return answer


# Creates a dataframe with questions
def question_df_creation(official):
    wide_format_df = pd.DataFrame(columns=['User', 'Question #', 'Answer'])

    # Create a dictionary that maps each question type to the corresponding column in the 'official' dataframe
    qtype_map = {'T/F': 'trufalse_mc_check', 'M-S': 'trufalse_mc_check', 'MC': 'trufalse_mc_check',
                 'MAT': 'answer_match_check', 'ORD': 'answer_match_check', 'SA': 'answer_match_check',
                 'MSA': 'answer_match_check', 'FIB': 'answer_match_check', 'WR': 'answer_check',
                 'LIK': 'answer_match_check'}

    # Map each question to its corresponding column using the 'qtype_map' dictionary
    qtype_col = official['Q Type'].map(qtype_map)

    # Assign values to each column in the 'wide_format_df' dataframe using vectorized operations
    wide_format_df['User'] = official['User']
    wide_format_df['Question #'] = official['label']
    wide_format_df['Answer'] = qtype_col

    # Apply relevant mask function to the relevant rows using a boolean mask
    # to avoid applying it to rows that don't need it
    mask = qtype_col == 'trufalse_mc_check'
    mask2 = qtype_col == 'answer_match_check'
    mask3 = qtype_col == 'answer_check'

    wide_format_df.loc[mask, 'Answer'] = official[official['Q Type'].isin(['T/F', 'MC', 'M-S'])]['# Responses'] \
        .apply(lambda x: trufalse_mc_check(x))

    mask_lik_mat_ord = official['Q Type'].isin(['LIK', 'MAT', 'ORD'])
    mask_sa_msa_fib = official['Q Type'].isin(['SA', 'MSA', 'FIB'])

    if mask_lik_mat_ord.any():
        responses = official[mask_lik_mat_ord]['# Responses']
        wide_format_df.loc[mask2 & mask_lik_mat_ord, 'Answer'] = responses.apply(lambda x: answer_match_check(x))

    if mask_sa_msa_fib.any():
        answer_match = official[mask_sa_msa_fib]['Answer Match']
        wide_format_df.loc[mask2 & mask_sa_msa_fib, 'Answer'] = answer_match.apply(lambda x: answer_match_check(x))

        wide_format_df.loc[mask3, 'Answer'] = official[official['Q Type'] == 'WR']['Answer'] \
            .apply(lambda x: answer_match_check(x))

    # Select the required columns and return the resulting dataframe
    return wide_format_df[['User', 'Question #', 'Answer']]


# Returns a list of question columns for the question parts dataframe
def question_df_column_list_creation(new_data):
    temp_columns = []
    q_list = []

    for question in new_data['Question #']:
        if question not in q_list:
            q_list.append(question)
            temp_columns.append(str(question))
        elif question in q_list:
            q_list = []
            break

    return temp_columns


# Creates a wide binary format of the question parts dataframe
def question_df_pivot(question_df, col_list):
    mod_question_df = pd.pivot_table(question_df, index=['User'],
                                     values='Answer', columns=['Question #'], aggfunc='first')
    mod_question_df = mod_question_df.reindex(col_list, axis=1)
    mod_question_df.columns = ['Q' + str(i) for i in mod_question_df.columns]
    return mod_question_df

survey_details = pd.read_csv("Final_Survey_-_Individual_Attempts__2___1_.csv")
survey_details_with_user_column = csvWithUserColumn(survey_details)
mod_survey_details = createSurveyOrder(survey_details_with_user_column)
question_df = question_df_creation(mod_survey_details)
question_df_col_list = question_df_column_list_creation(question_df)
wide_format_data = question_df_pivot(question_df, question_df_col_list)
print(wide_format_data.to_string())