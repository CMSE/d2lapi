This repository has been created as a part of CMSE495: Experiential Learning in Data Science at Michigan Sate University. This course is the capstone for the data science major and provides the opportunity for students to work in teams on a sponsored project as a culmination of the skills they have learned. 

The sponsor for the D2L Instructor API project is Michigan State University. The goal of this project is to build a data analysis API for the learning management system to provide more intuitive, insightful, and accesible student performance data to researchers and instructors. 

Here is a link to our proposal presentation video for more information about our project! 
https://mediaspace.msu.edu/media/MSUD2LInstructorAPI-CMSE495_Proposal_Presentation_Video/1_8nkuh5ul


Here is a link to our closed loop presentation video for more information about our project! 
https://mediaspace.msu.edu/media/MSUD2LInstructorAPI-CMSE495_Closed-Loop_Presentation_Video/1_lirffq9v


Our Figure Reproducibility milestone can be found in our repository in the section titled "FigureReproducibility." The notebook file is titled '20230326_MSUD2LInstructorAPI_FigureReproducibility.ipynb' and the accompanying data files are titled "FigureReproducibilityExampleData_QuizAttemptDetails.csv" and "FigureReproducibilityExampleData_AnswerKey.csv."

Group Members:
Olivia Sheng Qiu <qiuolivi@msu.edu>
Pranay Rahul Pentaparthy <pentapar@msu.edu>
Erin Elizabeth Sawyer <sawyerer@msu.edu>
Jonah Masaki Wehner <wehnerjo@msu.edu>
Jiaye Xie <xiejiaye@msu.edu>

Go to the INSTALL.md for further instructions for the API.
