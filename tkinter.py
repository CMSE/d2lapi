import tkinter as tk
from tkinter.filedialog import askopenfile
from tkinter import ttk
#from D2L import d2l

root = tk.Tk()
root.title('D2L API Test')

#root.iconbitmap('c:Users/cutepuppy.ico')    Here is a placeholder for an icon for our app

canvas = tk.Canvas(root) 
canvas.create_text(125,10,text="Select a CSV file from")
canvas.create_text(125,30,text="your computer to reformat")
canvas.create_text(125,50,text="D2L quiz/survey layout.")

tabControl = ttk.Notebook(root)
quiz_tab = ttk.Frame(tabControl)
survey_tab = ttk.Frame(tabControl)

tabControl.add(quiz_tab, text="Quiz data")
tabControl.add(survey_tab, text="Survey data")
tabControl.pack(expand=1,fill="both")

#ttk.Label(quiz_tab, text="Select a CSV file from your computer to reformat D2L quiz data.")
#ttk.Label(survey_tab, text="Select a CSV file from your computer to reformat survey data.")

def openfile():
  browse_text.set("Loading...")
  file = askopenfile(parent=root, mode='r', title="Choose a file.",filetypes=(("CSV","*.csv"),))
  if file is not None:
    print("File was loaded successfully!")
  else:
    print("Error! Loading file incomplete")
  
browse_text = tk.StringVar()
browse_button = tk.Button(root,textvariable=browse_text,
 command=lambda:openfile(),fg='#20bebe')
browse_text.set("Browse for files")

button_quit = tk.Button(root,text='Exit Program',command=root.quit, fg='#369110')

canvas.pack()
browse_button.pack()
button_quit.pack()

root.mainloop()

